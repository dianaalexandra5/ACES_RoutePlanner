# Route Planning Project
This is a Route Planning project for SDPT, PAO and RC courses, 1st year ACES. This project is based on Udacity C++ Nanodegree Project. This Route Planner uses A* algorithm to find the shortest path. Initially, it used OpenStreet Map data for and io2d rendering library to display the route on a map. 
## Dependencies:

- cmake >= 3.11.3;

- make >= 4.1

- gcc/g++ >= 7.4.0

- io2d: (only from graphics branch)    
    1.     Refresh apt:sudo apt update   
    2.     Install GCC: sudo apt install build-essential
    3.     Install CMake: sudo apt install cmake
    4.     Install Cairo: sudo apt install libcairo2-dev
    5.     Install graphicsmagick: sudo apt install libgraphicsmagick1-dev
    6.     Install libpng: sudo apt install libpng-dev

## Cloning

When cloning this project, be sure to use the `--recurse-submodules` flag. Using HTTPS:
```
git clone https://gitlab.com/dianaalexandra5/ACES_RoutePlanner.git --recurse-submodules
```
or with SSH:
```
git clone git@gitlab.com:dianaalexandra5/ACES_RoutePlanner.git --recurse-submodules
```

## Compiling and Running

### Compiling
To compile the project, first, create a `build` directory and change to that directory:
```
mkdir build && cd build
```
From within the `build` directory, then run `cmake` and `make` as follows:
```
cmake ..
make
```
### Running
The executables will be placed in the current directory. The project can run as follows:
```
./ACES_RoutePlanner -f ../map.osm
```
## Run the Docker

To build the container use from the docker_file directory:
```
docker build -f Dockerfile -t <name> .
```
To access the container:
```
docker run -it <name>
```

## Testing

For exercises that have unit tests, the project must be built with the approprate test cpp file. This can be done by passing a string with the `-DTESTING` flag in `cmake`. For example, from the build directory:
```
cmake -DTESTING="Enable" ..
make
```
The tests can then be run from the current directory as follows:
```
./unit-tests
```

### Output

After you compile the program, you can run the executables which are in the `build` directory. The executable requires path to the OpenStreetMap XML file which is placed in the project folder as `map.osm`. 

You need to enter start and end coordinates within 0-100. A sample output:
![alt text](https://github.com/arjun7965/Route-Planner/blob/master/images/Output.png)
