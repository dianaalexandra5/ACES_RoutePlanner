#include "gtest/gtest.h"
#include <iostream>
#include <vector>
#include <fstream>
#include "../src/route_model.h"
#include "../src/route_planner.h"


/* Declare the class to be used in testing*/

static std::optional<std::vector<std::byte>> ReadFile(const std::string &path)
{   
    std::ifstream is{path, std::ios::binary | std::ios::ate};
    if( !is )
        return std::nullopt;
    
    auto size = is.tellg();
    std::vector<std::byte> contents(size);    
    
    is.seekg(0);
    is.read((char*)contents.data(), size);

    if( contents.empty() )
        return std::nullopt;
    return std::move(contents);
}

std::vector<std::byte> ReadOSMData(const std::string &path) {
    std::vector<std::byte> osm_data;
    auto data = ReadFile(path);
    if( !data ) {
        std::cout << "Failed to read OSM data." << std::endl;
    } else {
        osm_data = std::move(*data);
    }
    return osm_data;
}

class RouteModelTest : public ::testing::Test {
  protected:
    std::string osm_data_file = "../map.osm";
    std::vector<std::byte> osm_data = ReadOSMData(osm_data_file);
    
    // Construct start_node and end_node as in the model.
    float start_x = 20;
    float start_y = 25;
    float end_x = 85;
    float end_y = 87;
    RouteModel model{osm_data};
    RoutePlanner route_planner{model, start_x, start_y, end_x, end_y};
    
    // start_node and end_node in percentage.
    float start_x_per = start_x/100;
    float start_y_per = start_y/100;
    float end_x_per = end_x/100;
    float end_y_per = end_y/100;
    RouteModel::Node* start_node = &model.FindClosestNode(start_x_per, start_y_per);
    RouteModel::Node* end_node = &model.FindClosestNode(end_x_per, end_y_per);
};

/*********** 1st Scenario Test Case --> Test path size to be zero initially **********/
TEST_F(RouteModelTest, RouteModelPathSize){
    EXPECT_EQ(model.path.size(), 0);
    EXPECT_EQ(model.Nodes().size(), 10754);
    EXPECT_EQ(model.Nodes().size(), model.SNodes().size());	
}

/*********** 2nd Scenario Test Case --> Test if Node class is defined correctly ********/
TEST_F(RouteModelTest, RouteModelNodeCorrect) {
    RouteModel::Node test_node = model.SNodes()[2];
    EXPECT_FLOAT_EQ(test_node.x, 1.2667634);
    EXPECT_FLOAT_EQ(test_node.y, 0.24275328);
    EXPECT_EQ(test_node.parent, nullptr);
    EXPECT_FLOAT_EQ(test_node.h_value, std::numeric_limits<float>::max());
    EXPECT_FLOAT_EQ(test_node.g_value, 0.0);
    EXPECT_FLOAT_EQ(test_node.visited, false);
    EXPECT_EQ(test_node.neighbors.size(), 0);
    //RouteModel::Node test_node_2 = RouteModel::Node();
}

/************** 3th Scenario Test Case --> Test a bad argument as input **********/
TEST_F(RouteModelTest, BadInputArg1) {
    // Construct test node (start_node or end_node) that exceeds the map limits :0 - 100 ( 0 - 1 in percentage) .
    float test_x = 1.1;
    float test_y = -0.5;
    ASSERT_THROW(&model.FindClosestNode(test_x, test_y),std::invalid_argument);
}
TEST_F(RouteModelTest, BadInputArg2) {
    // Construct test node (start_node or end_node) that exceeds the map limits :0 - 100 ( 0 - 1 in percentage) .
    float test_x = -1.8;
    float test_y = -0.5;
    ASSERT_THROW(&model.FindClosestNode(test_x, test_y),std::invalid_argument);
}
TEST_F(RouteModelTest, BadInputArg3) {
    // Construct test node (start_node or end_node) that exceeds the map limits :0 - 100 ( 0 - 1 in percentage) .
    float test_x = 2.5;
    float test_y = 0.5;
    ASSERT_THROW(&model.FindClosestNode(test_x, test_y),std::invalid_argument);
}

/************** 4rd Scenario Test Case --> Test the distance between 2 nodes ***********/
TEST_F(RouteModelTest, NodeDistance1) {
    RouteModel::Node test_node = model.SNodes()[2];
    RouteModel::Node test_node_2 = model.SNodes()[4];
    EXPECT_FLOAT_EQ(test_node.distance(test_node_2), 0.095130086);
}
TEST_F(RouteModelTest, NodeDistance2) {
    RouteModel::Node test_node = model.SNodes()[259];
    RouteModel::Node test_node_2 = model.SNodes()[445];
    EXPECT_FLOAT_EQ(test_node.distance(test_node_2), 1.0620294);
}
TEST_F(RouteModelTest, NodeDistance3) {
    RouteModel::Node test_node = model.SNodes()[5278];
    RouteModel::Node test_node_2 = model.SNodes()[10000];
    EXPECT_FLOAT_EQ(test_node.distance(test_node_2), 0.097401157);
}

/************** 5th Scenario Test Case --> Test FindClosestNode method ***********/
TEST_F(RouteModelTest, FindClosestNode1) {
    float x = 0.03;
    float y = 0.7;
    auto& test_node = model.FindClosestNode(x, y);
    EXPECT_FLOAT_EQ(test_node.x, 0.069425792);
    EXPECT_FLOAT_EQ(test_node.y, 0.6899457);

}
TEST_F(RouteModelTest, FindClosestNode2) {
    float x = 0.015;
    float y = 0.94;
    auto& test_node_2 = model.FindClosestNode(x, y);
    EXPECT_FLOAT_EQ(test_node_2.x, 0.026985561);
    EXPECT_FLOAT_EQ(test_node_2.y, 0.88937187);
}

/************** 6th Scenario Test Case --> Test AStarSearch method ***********/
TEST_F(RouteModelTest, AStarSearch) {
    route_planner.AStarSearch();
    EXPECT_EQ(model.path.size(), 32);
    // Start node is register last in path vector
    RouteModel::Node path_start = model.path[31];
    RouteModel::Node path_end = model.path[0];
    EXPECT_FLOAT_EQ(start_node->x, path_start.x);
    EXPECT_FLOAT_EQ(start_node->y, path_start.y);
    EXPECT_FLOAT_EQ(end_node->x, path_end.x);
    EXPECT_FLOAT_EQ(end_node->y, path_end.y);
    EXPECT_FLOAT_EQ(route_planner.GetDistance(), 772.97833);
}


/********** Call the Unit Testing Suite ***********/
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

