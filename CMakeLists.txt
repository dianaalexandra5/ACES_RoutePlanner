cmake_minimum_required(VERSION 3.0.0)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

set(CMAKE_ENABLE_EXPORTS ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

SET(GCC_COVERAGE_COMPILE_FLAGS "-fprofile-arcs -ftest-coverage")
SET(GCC_COVERAGE_LINK_FLAGS    "-lgcov")
SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}")
add_compile_options( "--coverage")
# Set project name
set (PROJECT_ID "ACES_RoutePlanner")
project(${PROJECT_ID})

# Add additional files directories 
add_subdirectory(thirdparty/pugixml)
add_subdirectory(thirdparty/googletest)

# Store all executables
file(GLOB project_SRCS src/*.cpp src/*.h)

# Add executables to project
add_executable(${PROJECT_ID} ${project_SRCS})

target_link_libraries(${PROJECT_ID}
    PUBLIC pugixml
)

# Unit testing section
if(NOT TESTING)
    set(TESTING "notest")
endif()

message("TESTING = ${TESTING}")

# create a library for unit tests
add_library(route_planner OBJECT src/route_planner.cpp src/model.cpp src/route_model.cpp)
target_include_directories(route_planner PRIVATE thirdparty/pugixml/src)

# Choose the test .cpp file.
if(${TESTING} STREQUAL "Enable")
	add_executable(unit-tests unit-tests/RoutePlannerTests.cpp)
#else()
       # If the user calls CMake with no flags, don't compile any test files.
	#add_executable(unit-tests)


target_link_libraries(unit-tests gtest_main route_planner pugixml gcov)
add_test(NAME unit-tests COMMAND unit-tests)

unset(TESTING CACHE)
endif()
